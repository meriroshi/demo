const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',  //Vendos mode ne "developement" per te optimizuar bundle.
  entry: {
    index: './src/index.js', // pika hyrese per aplikacionin tone
  },
  devtool: 'inline-source-map', // Gjeneron  source maps per debugging me te mire ne browser
    devServer: {
    static: './dist',
    },
 plugins: [new HtmlWebpackPlugin({
    template: './dist/index.html',
})],

  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
  },
  module: {
    rules:[
        { test: /\.css$/, use: [ 'style-loader', 'css-loader' ] }
    ]
},
 optimization: {
   runtimeChunk: 'single',
 },
};




//2023-11-10T11:00:00.000+00:00