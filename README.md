**To setup this project please follow these instructions:**

1. git clone https://gitlab.com/meriroshi/demo.git
2. cd demo
3. npm install 
4. npm start 

- If you want to contribute in this repository:

1. git checkout -b newBranch master

2. git status 
> with this command we check the status of our current branch after we make the changes. Here we can see what files have been modified
3. git add src/index.js

> here we add the files that were modified by us, if that is the file you changed; you can list more files in this command example: git add src/index.js src/style.css

4. git commit -m "your commit message"

5. git push --set-upstream origin newBranch


> Note: We did these steps using a combination of command line and Visual Studio Code. What I have listed above are all steps that are done using the command line. You can choose whatever method you like. 



